if ARGV.empty?
  puts 0
else
  puts (ARGV[1..-1].reduce(ARGV[0].to_i) do |left, right|
    if left % 2 == 1
      left + right.to_i
    else
      left - right.to_i
    end
  end)
end

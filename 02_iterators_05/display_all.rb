def display_all arr, num = 1
  arr.combination(num).map {|list| list.inject(&:+) }.sort
end

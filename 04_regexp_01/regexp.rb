SUBDOMAIN = "[a-z]{1,63}"
DOMAIN = "(?!.{254,})((#{SUBDOMAIN}\\.)*#{SUBDOMAIN})"
USER = '[a-z0-9\+\-\_\.%\*\\\\]{1,1023}'
EMAIL = /\A(?<user>#{USER})@(?<domain>#{DOMAIN})\Z/i

def my_count_words(words)
  Hash[
    words.reduce(Hash.new) do |left, right|
      left[right] ||= 0
      left[right] += 1
      left
    end.sort_by{|k, v| v}
  ]
end

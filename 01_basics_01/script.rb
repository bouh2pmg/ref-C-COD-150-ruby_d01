#!/usr/bin/env ruby

puts ARGV.join('').split('').count {|e| e.match(/\A[[:alnum:]]\Z/)}

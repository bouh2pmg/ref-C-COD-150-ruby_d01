def shows arr
  arr.map.with_index do |tuple, i|
    "(#{i}: #{tuple[0]}) -> (#{tuple[1].class}: #{tuple[1]})"
  end
end

#puts shows({a: 1, "b" => 1.2})

def increment arr
  arr.map do |e|
    if e.is_a? String
      e + "++"
    elsif e.is_a? Numeric
      e + 1
    else
      e
    end
  end
end

# puts increment([3, 4, "a", {}])

def display_all arr
  arr.each_with_index do |e, i|
    puts "#{i} -> (#{e.class}: #{e.to_s})"
  end
end

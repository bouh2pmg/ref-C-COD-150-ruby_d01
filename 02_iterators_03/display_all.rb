def display_all arr
  arr.each do |k, v|
    puts "#{k} -> (#{v.class}: #{v.to_s})"
  end
end
